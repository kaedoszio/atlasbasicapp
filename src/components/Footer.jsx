import React from 'react';
import '../assets/styles/Footer.css';

const Footer = () => (
  <footer className='footer'>
    <p>
      COPYRIGHT © ATLAS HOME TECHNOLOGIES S.A.S 2020. ALL RIGHTS RESERVED.
      POLITICAS DE PRIVACIDAD & TERMINOS Y CONDICIONES
    </p>
  </footer>
);

export default Footer;
