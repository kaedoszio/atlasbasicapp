/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import '../assets/styles/SelectCategories.css';

const SelectCategories = () => (
  <div className='background'>
    <div className='h-100 text-center'>
      <div className='row menu h-100'>
        <div className='col-12'>
          <div className='options_container'>
            <h1 className='title_label'>ENCUENTRA TU PROPIEDAD</h1>
            <div className='filter'>

              <label className='option_house filter-option'>
                <img src='https://cdn-atlas-cloud.s3.amazonaws.com/images/indexpage/house_icon.svg' className='image-white' alt='House Icon' />
                <figcaption>Casas</figcaption>
              </label>

              <label className='option_condominiun filter-option'>
                <img src='https://cdn-atlas-cloud.s3.amazonaws.com/images/indexpage/condominiun_icon.svg' className='image-white' alt='Condominiun Icon' />
                <figcaption>Condominios</figcaption>
              </label>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default SelectCategories;
