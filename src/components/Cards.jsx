import React, { Component } from 'react';
import '../assets/styles/Cards.css';

class Cards extends Component {

  ScrollToUp = (e) => {
    e.preventDefault();
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <div className='cards-container'>
        <div className='cards'>
          <div className='card-1 card'>
            <img src='https://cdn-atlas-cloud.s3.amazonaws.com/images/indexpage/search-filter-and-find.svg' alt='Busca,Filtra y Encuentra' />
            <h2 className='card-title'>Busca, Filtra y Encuentra</h2>
            <p>
              Atlas Home te permite visualizar toda la oferta inmobiliaria en tu ciudad. Camina en cada propiedad de forma
              digital y realiza tu cita de forma eficiente y 100% online.
            </p>
            <div className='card-footer'>
              <hr className='line-btn' />
              <button type='button' className='card-btn' onClick={this.ScrollToUp}>Filtra</button>
            </div>
          </div>
          <div className='card-2 card'>
            <img src='https://cdn-atlas-cloud.s3.amazonaws.com/images/indexpage/sell-your-property.svg' alt='Vende tu propiedad con nosotros' />
            <h2 className='card-title'>Anuncia tu propiedad con tecnología</h2>
            <p>
              Tecnología de recorridos vituales, acceso a clientela de la banca,
              clientes de calidad, economía y eficiencia.
            </p>
            <div className='card-footer'>
              <hr className='line-btn' />
              <button type='button' className='card-btn'>Vende</button>
            </div>
          </div>
          <div className='card-3 card'>
            <img src='https://cdn-atlas-cloud.s3.amazonaws.com/images/indexpage/get-credit-online.svg' alt='Solicita tu credito en linea' />
            <h2 className='card-title'>Solicita tu Crédito Online</h2>
            <p>
              Con una sola solicitud de crédito en Atlas Home puedes cotizar con todo el sistema financiero
              del país. Procesamos tu solicitud y te conectamos con el banco que más se adapta a tu perfil
              financiero.
            </p>
            <div className='card-footer'>
              <hr className='line-btn' />
              <button type='button' className='card-btn'>Aplica</button>
            </div>
          </div>
        </div>
        <br />
      </div>
    );
  }
}

export default Cards;
