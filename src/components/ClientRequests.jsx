import React from 'react';
import '../assets/styles/ClientRequests.css';

const ClientRequests = () => (
  <div className='text-center'>
    <h2 className='slogan'>
      &quot;Todo lo que necesitas en un solo lugar
      Atlas Home centraliza la industria finca raiz y financiera.&quot;
    </h2>
    <div className='btn-container'>
      <button className='btn btn-credit' type='button'>Solicitar Crédito Hipotecario</button>

      <button className='btn btn-sell' type='button'>Vende tu propiedad con Atlas Home</button>
    </div>
    <br />
    <hr className='line' />
    <br />
  </div>
);

export default ClientRequests;

