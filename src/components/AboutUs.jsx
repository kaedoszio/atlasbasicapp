import React from 'react';

import '../assets/styles/AboutUs.css';

const AboutUs = () => (
  <div className='about'>
    <div className='about-titles'>
      <h2>¿Quienes Somos?</h2>
      {/* <hr className='line' /> */}
      <button type='button' className='card-btn about-btn'>Conoce más</button>
      {/* <h3>Conoce nuestra historia</h3>
      <div className='youtube-iframe'>
        <iframe
          title='Youtube Iframe'
          src='https://www.youtube.com/embed/54xHEehrjww'
          frameBorder='0'
          allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
          allowFullScreen=''
          data-hj-allow-iframe=''
        />
      </div> */}
    </div>
  </div>
);

export default AboutUs;
