import React from 'react';
import '../assets/styles/Header.css';

const Header = () => (
  <nav role='navigation' className='topnav'>

    <div className='logo'>
      <img src='https://cdn-atlas-cloud.s3.amazonaws.com/images/indexpage/logo.svg' className='logo--image' alt='' />
    </div>

    <div className='icon'>
      <div className='icon--container-anchor'>
        <a href='www.google.com'>
          <i className='fa fa-bars' />
        </a>
      </div>
    </div>

  </nav>
);

export default Header;
