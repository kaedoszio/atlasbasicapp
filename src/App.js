import React, { Component } from 'react';
import Header from './components/Header';
import SelectCategories from './components/SelectCategories';
import ClientRequests from './components/ClientRequests';
import Cards from './components/Cards';
import AboutUs from './components/AboutUs';
import Footer from './components/Footer';

import './App.css';

class App extends Component {

  render() {
    return (
      <div id='App'>
        <Header />
        <SelectCategories />
        <ClientRequests />
        <Cards />
        <AboutUs />
        <Footer />
      </div>
    );
  }
};

export default App;
