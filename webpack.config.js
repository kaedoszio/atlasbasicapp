const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");


module.exports = (env) => {

    const plugins = [
        new HtmlWebpackPlugin({
            template:'./public/index.html',
            filename:'./index.html'
        })
    ]
    //Plugins para entornos de produccion
    if(env.NODE_ENV === 'production'){
        plugins.push(
            new CleanWebpackPlugin(),
            new MiniCssExtractPlugin({filename:'[name].[contenthash:5].css'}),
            new FixStyleOnlyEntriesPlugin(),
            new OptimizeCSSAssetsPlugin({})
        )
    }else{
        plugins.push(
            new MiniCssExtractPlugin({
                filename: '[name].css'
            })
        )
    }

    //Agregar Hashes si es un entorno de produccion
    const Filename = env.NODE_ENV === 'production' ? 'bundle.[chunkhash:5].js' : 'bundle.js';

    return{
    entry: './src/index.js',
    output:{
        path: path.resolve(__dirname,'public'),
        filename: Filename
    },
    resolve:{
        extensions:['.js','.jsx']
    },
    module:{
        rules:[
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use:{
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use:{
                    loader: "html-loader"
                }
            },
            {
                test: /\.css$/,
                use:[MiniCssExtractPlugin.loader, 'css-loader']
            }
        ] 
    },
    // devServer: {
    //     allowedHosts: [
    //         '192.168.0.6'
    //     ]
    // },
    plugins,
    optimization: {
        splitChunks: {
            cacheGroups: {
              commons: {
                test: /[\\/]node_modules[\\/]/,
                name: "vendor",
                chunks: "initial",
              },
            },
          },
          runtimeChunk: {
            name: "manifest",
          },
      },

    }
}